MACE
-------------
Miscreated Advanced Crafting Enhaced

ModID: 2480835664

Little tweaked crafting...

Added:
-------------------------------------------------
Sleeping Bag - Placeable Respawner

Added recipes:
-------------------------------------------------
Hidden Stash

Medical Heal Joint

Medical Antibiotics Joint

Medical Antiradiation Joint

ZipZap Papers

Cannabis Vookie Dough

Cannabis Butter

Pie

Cookie Dough

Iron Ingot from 2 Scrap Metal

Leather repair kit from 2 WolfSkin

-------------------------------------------------

And Baking Guide
________________________________________________

![MACE](https://gitlab.com/miscreated/MACE/MACE.jpg)

________________________________________________

Medical Cannabis thanks to:
https://github.com/Bealze/Gamer-Love

Hidden Stash and Sleeping Bag thanks to:
https://steamcommunity.com/id/PitiViers

Steam Mod Link:
https://steamcommunity.com/sharedfiles/filedetails/?id=2480835664

Feel free to contribute to project:
https://gitlab.com/miscreated/MACE

Tweak It! Share it! Enjoy it!

Check also MAC version:

https://steamcommunity.com/sharedfiles/filedetails/?id=2479558100
or on GitLab:
https://gitlab.com/miscreated/MAC