--------------------------------------------------------------------------------------------

    local newWeather = {
      name="DustStorm_Peak",
      probability=0,
      danger=1,
      duration={2, 4},
	  continue={
        ["DustStorm_Outro"] = 100, -- Continue pattern starts at fade out of this pattern, so blending works
      }, 
	  ramp={0.2, 0.2}, -- use 15% fade-in and 20% fade-out for fog
      modifiers = { -- values added to current baseline (faded in and out)
        --          outside, inside, underground, underwater
        humidity=   {-2.2,     -1.1,                },
        light=      {-0.4,    -0.25,               },
		torpidity=  {0.5,                         },
		physical=   {0.5,       	              },
        wind=       {80,      20,     2,      1   },
      },
       entities=
      {
        BellTriggers[1],BellTriggers[2],BellTriggers[3],
		{
          class="ParticleEffect",
          properties=
          {
            ParticleEffect= {"weather.ash.slow_ash"},
            PulsePeriod= {5},
            Strength={1.0}, -- Strength={1.0, 1.0, 0.0},
          },
        },
		
		{
          class="ParticleEffect",
          properties=
          {
            ParticleEffect= {"weather.snow.blizzard"}, 
            PulsePeriod= {20}, 
            Strength={1.0}, -- Strength={0.0, 1.0},
          },
        },
		
		{
          class="WindArea", -- cant be interpolated unless physicalized per frame, but not really needed
          properties=
          {
            --				start, full(default=start), end(default=start)
            bActive=		{1},
            bEllipsoidal=	{0},
            Speed=			{4},
            Size=			{{ x=200,y=200,z=200 }},
          },
          OnCustomUpdate=windMover,
        },
		},
      audio=
      {
        {
          class="AudioAreaAmbience",
          trigger="Play_wind",
          rtpc="wind_speed",
          rtpcValue=10.0,
        },
		{
          class="AudioAreaAmbience",
          trigger="Play_nuclear_winter",
          rtpc="freeze_strength",
          rtpcValue=0.35,
        },
      },
      tod=
      {
        [etod.PARAM_SUN_INTENSITY]=                 {0.6, method=emix.MULTIPLY, min=100}, -- lux is logaritmic so half it by decreasing by factor 10
        [etod.PARAM_FOG_RADIAL_COLOR_MULTIPLIER]=   {0.1},
        [etod.PARAM_SUN_SPECULAR_MULTIPLIER]=       {0.05},
        [etod.PARAM_SUN_COLOR]=                     {{ x=255/255, y=200/255, z=130/255 }, constraint=econ.DARKEN, }, -- orange light
        [etod.PARAM_NIGHSKY_MOON_COLOR]=            {{ x=255/255, y=200/255, z=130/255 }, constraint=econ.DARKEN, }, -- orange light

        -- Non-Volumetric Fog
        [etod.PARAM_VOLFOG_GLOBAL_DENSITY]=         {5.0, undergroundFactor=_uff, insideFactor=_iff},
        [etod.PARAM_VOLFOG_DENSITY]=                {0.9, undergroundFactor=_uff, insideFactor=_iff},
        [etod.PARAM_FOG_COLOR_MULTIPLIER]=          {0.9},
        [etod.PARAM_FOG_COLOR2_MULTIPLIER]=         {0.9},

        [etod.PARAM_FOG_COLOR]=                     {{ x=180/255, y=140/255, z=45/255 }, constraint=econ.DARKEN }, -- bottom
        [etod.PARAM_FOG_COLOR2]=                    {{ x=255/255, y=200/255, z=50/255 }, constraint=econ.DARKEN }, -- top
        [etod.PARAM_VOLFOG_RAMP_INFLUENCE]=         {0.01, undergroundFactor=_uff, insideFactor=_iff},
        [etod.PARAM_VOLFOG_HEIGHT_OFFSET]=          {0.1, undergroundFactor=_uff},
        [etod.PARAM_VOLFOG_FINAL_DENSITY_CLAMP]=        {0.997, undergroundFactor=_uff, insideFactor=_iff}, -- allow look through at least a bit always

        -- Volumetric Fog
        --[etod.PARAM_VOLFOG2_GLOBAL_DENSITY]=      {2.0, method=emix.QUART},

        [etod.PARAM_COLORGRADING_FILTERS_PHOTOFILTER_COLOR]=    {{ x=255/255, y=200/255, z=50/255 }, constraint=econ.BRIGTHEN, },
        [etod.PARAM_COLORGRADING_FILTERS_PHOTOFILTER_DENSITY]=  {0.5, undergroundFactor=_ufc, insideFactor=_ifc},

        [etod.PARAM_COLORGRADING_FILTERS_GRAIN]=    {1.0},
        [etod.PARAM_COLORGRADING_DOF_FOCUSRANGE]=   {10, undergroundFactor=_ufd, insideFactor=_ifd},
        [etod.PARAM_COLORGRADING_DOF_BLURAMOUNT]=   {1.0, method=emix.ADDITIVE, undergroundFactor=_ufd, insideFactor=_ifd},

        [etod.PARAM_SUN_RAYS_VISIBILITY]=       {0.5},
      },
    }
	
	table.insert(Weather.patterns, newWeather)

--------------------------------------------------------------------------------------------