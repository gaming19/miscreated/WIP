ChatCommands["!s"] = function(playerId, command)
	Log(">> !s - %s", command)

	local player = System.GetEntity(playerId)
  
  -- Only allow the following SteamId to invoke the command 
  local steamid = player.player:GetSteam64Id()
  local allowCommand = steamid == "76561198799309787" -- change this to some valid admin's Steam64 id
  
  -- or through faction restrictions
  -- allowCommand = allowCommand or nil ~= string.match(System.GetCVar("g_gameRules_faction4_steamids"), steamid)
  
  -- or through actual current faction
  -- allowCommand = allowCommand or 4 == player.actor:GetFaction() -- faction 0 to 7 (same numbering as cvars)
  
	if allowCommand then
		local vForwardOffset = {x=0,y=0,z=0}
		FastScaleVector(vForwardOffset, player:GetDirectionVector(), 3.0)

		local vSpawnPos = {x=0,y=0,z=0}
		FastSumVectors(vSpawnPos, vForwardOffset, player:GetWorldPos())
		
		local spawnParams = {}

    spawnParams.class = command

		spawnParams.name = spawnParams.class
		spawnParams.position = vSpawnPos

		

		 Log("Spawning - %s", command)

		 local spawnedEntity = System.SpawnEntity(spawnParams)

	 	if not spawnedEntity then

		 Log("could not be spawned")
		end
	end
end